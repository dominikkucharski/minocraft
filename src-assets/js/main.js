/*******************************************************************************

  Project: Licht und Wärme
  Author: XXX
  Date: XX.XXXX

********************************************************************************

  1. Scroll to top
  2. Off canvas menu
  3. EU Cookie
  4. Dropdown Menu
  5. Lightbox for images and videos
  6. Responsive Videos
  7. Scoll Animations Init
  8. Object Fit Images
  9. Framework Scripts
  10. Contact Form 7 Fix
  11. Accordion
  12. Map
  13. Equal Heights
  14. Showcase slider
  15. Expandable Text
  16. Image Gallery
  17. Showcase Animation
  18. Meta

*******************************************************************************/

jQuery( document ).ready(function($) {

/* 1. Scroll to top
------------------------------------------------------------------------------*/

  $(document).on( 'scroll', function(){
    if ($(window).scrollTop() > 200) {
      $('.o-scroll-to-top').addClass('show');
    } else {
      $('.o-scroll-to-top').removeClass('show');
    }
  });

  $('.o-scroll-to-top').on('click', function(event) {
    event.preventDefault();
    var verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
    var element = $('body');
    var offset = element.offset();
    var offsetTop = offset.top;
    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
  });

/* 2. Off canvas menu
------------------------------------------------------------------------------*/

  var wp_admin_bar_enabled = false;

  if($('#wpadminbar').length > 0) {
    wp_admin_bar_enabled = true;
  }

  if(wp_admin_bar_enabled) {
    $('.o-site-header').css('top', $('#wpadminbar').outerHeight() + 'px');
  }

  var mainHeader = $('.o-site-header'),
      headerHeight = mainHeader.height();

  //set scrolling variables
  var scrolling = false,
    previousTop = 0,
    currentTop = 0,
    scrollDelta = 10,
    scrollOffset = 150;

  $(window).on('scroll', function(){
    if( !scrolling ) {
      scrolling = true;
      (!window.requestAnimationFrame) ? setTimeout(autoHideHeader, 250) : requestAnimationFrame(autoHideHeader);
    }
  });

  $(window).on('resize', function(){
    headerHeight = mainHeader.height();
  });

  function autoHideHeader() {
    var currentTop = $(window).scrollTop();
    checkSimpleNavigation(currentTop);
    previousTop = currentTop;
    scrolling = false;
  }

  function checkSimpleNavigation(currentTop) {
      if (previousTop - currentTop > scrollDelta) {
        //if scrolling up...
        mainHeader.removeClass('o-site-header--hidden');
        mainHeader.addClass('o-site-header--green');
      } else if( currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
        //if scrolling down...
        if(!($('#site-wrapper').hasClass('show-nav'))) {
          mainHeader.addClass('o-site-header--hidden');
        }
      }
      if(currentTop <= 0 || (wp_admin_bar_enabled && currentTop == $('#wpadminbar').outerHeight()) || currentTop <= $('.b-showcase').height() / 2.5) {
        mainHeader.removeClass('o-site-header--green');
      }
  }

  $('.toggle-nav').click(function() {
    // Calling a function in case you want to expand upon this.
    toggleNav();
  });

  function toggleNav() {
    if ($('#site-wrapper').hasClass('show-nav')) {
      // Do things on Nav Close
      $('#site-wrapper').removeClass('show-nav');
    } else {
      // Do things on Nav Open
      $('#site-wrapper').addClass('show-nav');
    }
  }

  // Fuction for hiding mobile navigation
  function hideNav() {
    if ($('#site-wrapper').hasClass('show-nav')) {
      $('#site-wrapper').removeClass('show-nav');
    }
  }

  // Hide navigation when user pressed Escape key on keyboard
  $(document).on('keyup', function(e){
    if (e.keyCode === 27) {
      if ($('#site-wrapper').hasClass('show-nav')) {
        toggleNav();
      }
    }
  });

  // Hide mobile navigation when user increases browser size
  var sizeMenuVisible = window.matchMedia("(min-width: 768px)");

  sizeMenuVisible.addListener(function(mq){
    if(mq.matches) {
      hideNav();
    }
  });

  $('ul.mob-menu li').has('ul').addClass('parentLi');
  $('ul.mob-menu li ul').addClass('sub-menu');

  $('ul.mob-menu .sub-menu').hide();

  $('ul.mob-menu .parentLi > a').on('click', function(event){
    event.preventDefault();
    $(this).toggleClass('expanded');
    $(this).parent().find('ul.sub-menu').slideToggle();
  });

/* 3. EU Cookie
------------------------------------------------------------------------------*/

  // Variables
  var expires_date = new Date('December 31, 2099 23:59:59');
  var cookie_notice_height = $('.o-cookie-bar').height();

  // Update variables and padding after screen resize
  $(window).on("debouncedresize", function( event ) {
    cookie_notice_height = $('.o-cookie-bar').height();
    if(!(Cookies.get('eu-cookie-law-licht_und_waerme') == 'accepted' || Cookies.get('eu-cookie-law-licht_und_waerme') == 'refused')) {
      $('.site-footer .wrap').css('padding-bottom', cookie_notice_height + 'px');
    }
  });

  // Show cookie notice if it's not configured
  if(Cookies.get('eu-cookie-law-licht_und_waerme') == 'accepted' || Cookies.get('eu-cookie-law-licht_und_waerme') == 'refused') {
    $('body').removeClass('o-cookie-bar--not-decided');
  }
  else {
    $('.site-footer .wrap').css('padding-bottom', cookie_notice_height + 'px');
    $('body').addClass('o-cookie-bar--not-decided');
  }

  // Hide cookie if it's accepted
  $('.js-accept-cookie').on('click', function(e){
    e.preventDefault();
    Cookies.set('eu-cookie-law-licht_und_waerme', 'accepted', { expires: expires_date });
    $('body').removeClass('o-cookie-bar--not-decided');
  });

  // Disable scripts and hide notice
  $('.js-refuse-cookie').on('click', function(e){
    e.preventDefault();
    if(theme_settings.cookie_popup == 1) {
      $.magnificPopup.open({
        items: {
            src: '<div class="white-popup">' + theme_settings.cookie_popup_text + '</div>',
            type: 'inline'
        },
        mainClass: 'mfp-with-fade',
        removalDelay: 160
      });
    }
    else {
      console.log('Tracking scripts and cookies are disabled.');
    }
    Cookies.set('eu-cookie-law-licht_und_waerme', 'refused', { expires: expires_date });
    // Opt out functions
    // doOptout();
    $('body').removeClass('o-cookie-bar--not-decided');
  });

/* 4. Dropdown Menu
------------------------------------------------------------------------------*/

  $('.primary-menu .menu-item-has-children > a').on('click', function(e){
    e.preventDefault();
    $(this).parent().toggleClass('opened');
  });

  // Hide dropdown after click outside the menu
  $(document).on('click', function(event) {
    if (!$(event.target).closest('.primary-menu .menu-item-has-children a').length) {
      $('.primary-menu .menu-item-has-children').removeClass('opened');
    }
  });

/* 5. Lightbox for images and videos
------------------------------------------------------------------------------*/

  // $('a[href$=jpg], a[href$=jpeg], a[href$=jpe], a[href$=png], a[href$=gif]').magnificPopup({
  //   type: 'image',
  //   removalDelay: 500,
  //   mainClass: 'mfp-with-fade',
  //   closeOnContentClick: true,
  //   midClick: true,
  //   gallery: {
	// 		enabled: true,
	// 		navigateByImgClick: true,
	// 		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	// 	}
  // });

  $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-with-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    iframe: {
			patterns: {
        youtube: {
          index: 'youtube.com/',
          id: function(url) {
                  var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
                  if ( !m || !m[1] ) return null;
                  return m[1];
              },
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
				youtube_short: {
				  index: 'youtu.be/',
				  id: 'youtu.be/',
				  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
				}
			}
		}
  });

/* 6. Responsive Videos
/*******************************************************************************/

  fitvids({
    // players: 'iframe[src*="example.com"]'
  });

/* 7. Scoll Animations Init
/*******************************************************************************/

  AOS.init({
    disable: 'mobile',
    once: true
  });

  window.addEventListener('load', AOS.refresh);

  function refreshAOS() {
    AOS.refresh();
  }

  $('img').on('load', _.throttle(refreshAOS, 1000));

/* 8. Object Fit Images
/*******************************************************************************/

  objectFitImages();

/* 9. Framework Scripts
/*******************************************************************************/

//= include framework-scripts.js

/* 10. Contact Form 7 Fix
/*******************************************************************************/

  $('.wpcf7-form p:empty').remove();

/* 11. Accordion
/*******************************************************************************/

  // Hide all accordions
  var allPanels = $('.c-accordion dd').hide();
  var allTitles = $('.c-accordion dt');

  $('.c-accordion dt > a').click(function(e) {
  e.preventDefault();
  $this = $(this);
  $title = $this.parent();
  $target =  $this.parent().next();

  if(!$target.hasClass('c-accordion__text--active')){
    allPanels.removeClass('c-accordion__text--active').slideUp();
    allTitles.removeClass('c-accordion__subtitle--active');
    $target.addClass('c-accordion__text--active').slideDown();
    $title.addClass('c-accordion__subtitle--active');
  }
  else {
    allPanels.removeClass('c-accordion__text--active').slideUp();
    allTitles.removeClass('c-accordion__subtitle--active');
  }
  return false;
  });

/* 12. Map
/*******************************************************************************/

  $.each($('.c-map'), function(){

    var lat = $(this).data('lat');
    var long = $(this).data('long');
    var zoom = $(this).data('zoom');

    L.Icon.Default.imagePath = theme_settings.template_dir + '/assets/img/leaflet/';
    var mymap = new L.map($(this)[0], {
    scrollWheelZoom: false
    }).setView([lat, long], zoom);

    // create custom icon
    var pinIcon = L.icon({
        iconUrl: theme_settings.template_dir + '/assets/img/leaflet/pin.svg',
        shadowUrl:  theme_settings.template_dir + '/assets/img/leaflet/marker-shadow.png',
        iconSize: [40, 50], // size of the icon
        iconAnchor:   [16, 50],
        shadowAnchor: [10, 39],
        popupAnchor:  [5, -48]
    });

    // Open Street Map - Wikimedia
    L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
        attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
        maxZoom: 19
    }).addTo(mymap);

    if($(this).data('popup')) {
      L.marker([lat, long], {icon: pinIcon}).addTo(mymap)
      .bindPopup($(this).data('popup'))
      .openPopup();
    }

    // Open Street Map
    /*
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        maxZoom: zoom
    }).addTo(mymap);
    */

    // Open Street Map - Black and White
    /*
    L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        maxZoom: zoom
    }).addTo(mymap);
    */

    // Mapbox - remember to change accessToken
    /*
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: zoom,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiYWRhbXBpbGNoIiwiYSI6ImNqbTY0aGlzeTB0eTEza280dGVpcjVnemMifQ.zZjr5GSWWpe4lYGBjvuz1A'
    }).addTo(mymap);
    */

    mymap.on('click', function() {
    if (mymap.scrollWheelZoom.enabled()) {
      mymap.scrollWheelZoom.disable();
      }
      else {
      mymap.scrollWheelZoom.enable();
      }
    });

  });

/* 13. Equal Heights
------------------------------------------------------------------------------*/

  // Example:
  // $('.something').matchHeight();

  // Match Height compatibility with Lazy Load techniques
  function refreshMatchHeight() {
    $.fn.matchHeight._update();
  }
  $('img').on('load', _.throttle(refreshMatchHeight, 1000));

/* 14. Showcase slider
------------------------------------------------------------------------------*/

  var swiper = new Swiper('.b-showcase--slider', {
      loop: false,
      effect: 'fade',
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      },
      keyboard: true,
      autoplay: {
        delay: 5000,
      },
      speed: 1000
    });

/* 15. Expandable Text
------------------------------------------------------------------------------*/

  $('.show-more').click(function(){
    $(this).prev().slideToggle();
    $(this).toggleClass('show-more--active');
  });

/* 16. Image Gallery
------------------------------------------------------------------------------*/

  $(".js-open-photo-gallery").lightGallery({
    thumbnail: true,
    download: false,
    mode: 'lg-fade',
    startClass: 'lg-start-fade',
    counter: false
  });

/* 17. Showcase Animation
------------------------------------------------------------------------------*/

  if(window.scrollY == 0) {
    if($('.b-showcase--default').length > 0 || $('.b-showcase--small').length > 0 || $('.b-showcase--slider').length > 0) {
      if($('.b-showcase--default').length > 0 || $('.b-showcase--small').length > 0) {
        $('.b-showcase__bg-img').on('load', function(){
          $(this).closest('body').addClass('launch-first-animation');
        });
      }

      if($('.b-showcase--slider').length > 0) {
        console.log($('.b-showcase__bg-img--number-0').attr('class'));
        $('.b-showcase__bg-img--number-0').on('load', function(){
          $(this).closest('body').addClass('launch-first-animation');
        });
      }
    }
    else {
      $('body').addClass('launch-first-animation');
    }
  }
  else {
    $('body').addClass('launch-first-animation');
  }

  if($('.b-showcase').hasClass('b-showcase--no-loader')) {
    $(this).closest('body').addClass('launch-first-animation');
  }


/* 18. Meta
------------------------------------------------------------------------------*/

  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    event.preventDefault();
    // Get the hash. In this example, "#myDestinationAnchor".
    var targetSelector = this.hash;
    var $target = $(targetSelector);

    // Animate the scroll to the destination...
    $('html, body').animate(
        {
            scrollTop: $target.offset().top // Scroll to this location.
        }, {
            // Set the duration long enough to allow time
            // to lazy load the elements.
            duration: 1000,

            // At each animation step, check whether the target has moved.
            step: function( now, fx ) {

                // Where is the target now located on the page?
                // i.e. its location will change as images etc. are lazy loaded
                var newOffset = $target.offset().top;

                // If where we were originally planning to scroll to is not
                // the same as the new offset (newOffset) then change where
                // the animation is scrolling to (fx.end).
                if(fx.end !== newOffset)
                    fx.end = newOffset;
            }
        }
    );
  });

}); // end of jQuery code
