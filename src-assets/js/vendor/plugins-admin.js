// Imports
//= include ../../../node_modules/jquery-match-height/dist/jquery.matchHeight.js
//= include ../../../node_modules/object-fit-images/dist/ofi.js
//= include ../../../node_modules/fitvids/dist/fitvids.js
//= include ../../../node_modules/slick-carousel/slick/slick.js
//= include ../../../node_modules/leaflet/dist/leaflet.js
//= include ../../../node_modules/swiper/js/swiper.js

// Bootstrap - in most cases you do not need whole framework
// Enable only required parts
// var $ = window.jQuery; // uncomment to use Bootstrap JavaScripts in WP
// include ../../../node_modules/bootstrap/js/dist/alert.js
// include ../../../node_modules/bootstrap/js/dist/button.js
// include ../../../node_modules/bootstrap/js/dist/carousel.js
// include ../../../node_modules/bootstrap/js/dist/collapse.js
// include ../../../node_modules/bootstrap/js/dist/dropdown.js
// include ../../../node_modules/bootstrap/js/dist/index.js
// include ../../../node_modules/bootstrap/js/dist/modal.js
// include ../../../node_modules/popper.js/dist/popper.min.js
// include ../../../node_modules/bootstrap/js/dist/popover.js
// include ../../../node_modules/bootstrap/js/dist/scrollspy.js
// include ../../../node_modules/bootstrap/js/dist/tab.js
// include ../../../node_modules/bootstrap/js/dist/tooltip.js
// include ../../../node_modules/bootstrap/js/dist/util.js
