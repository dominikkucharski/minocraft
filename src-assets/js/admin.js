jQuery( document ).ready(function($) {
  // Map
  $.each($('.c-map'), function(){

    var lat = $(this).data('lat');
    var long = $(this).data('long');
    var zoom = $(this).data('zoom');

    L.Icon.Default.imagePath = 'wp-content/themes/licht-und-waerme/assets/img/leaflet/';
    var mymap = new L.map($(this)[0], {
    scrollWheelZoom: false
    }).setView([lat, long], 16);

    // create custom icon
    var pinIcon = L.icon({
        iconUrl: 'wp-content/themes/licht-und-waerme/assets/img/leaflet/pin.svg',
        shadowUrl:  'wp-content/themes/licht-und-waerme/assets/img/leaflet/marker-shadow.png',
        iconSize: [40, 50], // size of the icon
        iconAnchor:   [16, 50],
        shadowAnchor: [10, 39],
        popupAnchor:  [5, -48]
    });

    // Open Street Map - Wikimedia
    L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
        attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
        maxZoom: 19
    }).addTo(mymap);

    if($(this).data('popup')) {
      L.marker([lat, long], {icon: pinIcon}).addTo(mymap)
      .bindPopup($(this).data('popup'))
      .openPopup();
    }

    // Open Street Map
    /*
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        maxZoom: zoom
    }).addTo(mymap);
    */

    // Open Street Map - Black and White
    /*
    L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        maxZoom: zoom
    }).addTo(mymap);
    */

    // Mapbox - remember to change accessToken
    /*
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: zoom,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiYWRhbXBpbGNoIiwiYSI6ImNqbTY0aGlzeTB0eTEza280dGVpcjVnemMifQ.zZjr5GSWWpe4lYGBjvuz1A'
    }).addTo(mymap);
    */

    mymap.on('click', function() {
    if (mymap.scrollWheelZoom.enabled()) {
      mymap.scrollWheelZoom.disable();
      }
      else {
      mymap.scrollWheelZoom.enable();
      }
    });

  });
});
