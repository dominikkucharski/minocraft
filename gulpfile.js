// Imports
var {gulp, src, dest, watch, series, parallel} = require('gulp');
var sass         = require('gulp-sass'); // Loading Sass preprocessor
var sassGlob     = require('gulp-sass-glob');
var postcss      = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var cssnano      = require("cssnano");
var gulpif       = require('gulp-if');
var sourcemaps   = require('gulp-sourcemaps');
var include      = require("gulp-include");
var uglify       = require('gulp-uglify');
var jshint       = require('gulp-jshint');
var cache        = require('gulp-cache');
var imagemin     = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var imageminZopfli = require('imagemin-zopfli');
var imageminMozjpeg = require('imagemin-mozjpeg'); //need to run 'brew install libpng'
var imageminGiflossy = require('imagemin-giflossy');
var fileinclude  = require('gulp-file-include');
var browserSync  = require('browser-sync').create(); // Creating BrowserSync
var fs           = require('fs');

// Setup enviroment type (Static, Wordpress)
try {
  var options = require('./options.js');
  // Local enviroment setup override
  var isStatic       = options.isStatic;
  var isWP           = options.isWP;
  var isCraft        = options.isCraft;
  var templateNameWP = options.templateNameWP;
} catch(err) {
  // Default enviroment setup (in most cases for deployment)
  var isStatic       = false;  // Static website
  var isWP           = false;   // WordPress Project
  var isCraft        = true;   // Craft CMS Project
  var templateNameWP = 'goodstart';
}

// Local server
var server = function(done) {
  // Run server
  if(isStatic) {
    browserSync.init({
      server: {
        baseDir: "./static/dist"
      }
    });
  }
  else {
    browserSync.init({
      proxy: options.serverLink
    });
  }

  // Signal completion
	done();
}

// Reload the browser when files change
var reloadBrowser = function (done) {
	browserSync.reload();
	done();
};

// Compile source HTML files
var html = function(done) {
  return src('static/html/*.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file',
    }))
    .pipe(gulpif(isStatic,dest('static/dist')));
}

// Compile main Sass files
var stylesMain = function(done) {
  return src(['src-assets/sass/main.sass'])
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sassGlob())
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulpif(isStatic,dest('./static/dist/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(gulpif(isCraft,dest('./public_html/assets/css')))
    .pipe(browserSync.stream({match: '**/*.css'}));
}

var stylesEditor = function(done) {
  return src(['src-assets/sass/editor.sass'])
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sassGlob())
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulpif(isCraft,dest('./public_html/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(browserSync.stream({match: '**/*.css'}));
}

// Compile main Sass files
var stylesPartials = function(done) {
  return src(['src-assets/sass-partials/**/*.sass'])
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sassGlob())
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulpif(isStatic,dest('./static/dist/assets/css')))
    .pipe(gulpif(isCraft,dest('./public_html/assets/css/partials')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css/partials')))
    .pipe(browserSync.stream({match: '**/*.css'}));
}

// Scripts tasks
var scriptsVendor = function() {
  return src('src-assets/js/vendor/*.js')
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(include())
        .on('error', console.log)
      .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulpif(isStatic,dest('./static/dist/assets/js/vendor')))
    .pipe(gulpif(isCraft,dest('./public_html/assets/js/vendor')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js/vendor')));
}

// JavaScript partials
var scriptPartials = function(done) {
  return src('src-assets/js-partials/**/*.js')
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(include())
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
      .pipe(uglify())
    .pipe(sourcemaps.write('../js'))
    .pipe(gulpif(isStatic,dest('./static/dist/assets/js/partials')))
    .pipe(gulpif(isCraft,dest('./public_html/assets/js/partials')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js/partials')));
}

// Check main JS file syntax & minify it
var scriptMain = function(done) {
  return src('src-assets/js/main.js')
    .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(include())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(uglify())
    .pipe(sourcemaps.write('../js'))
    .pipe(gulpif(isStatic,dest('./static/dist/assets/js')))
    .pipe(gulpif(isCraft,dest('./public_html/assets/js')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js')));
}

// Copy fonts to appropriate folder
var fonts = function(done) {
  return src(['src-assets/fonts/**/*.*'])
    .pipe(gulpif(isStatic,dest('./static/dist/assets/fonts')))
    .pipe(gulpif(isCraft,dest('./public_html/assets/fonts')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/fonts')));
}

// // Optimize all images
var images = function(done) {
  return src('src-assets/img/**/*')
  .pipe(cache(imagemin([
    imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { cleanupIDs: true }
        ]
    }),
    ])))
  .pipe(gulpif(isStatic,dest('./static/dist/assets/img')))
  .pipe(gulpif(isCraft,dest('./public_html/assets/img')))
  .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/img')));
}

// Watch for changes
var watchSource = function (done) {
  watch('src-assets/sass/**/*.+(sass|scss)', series(stylesMain));
  watch('src-assets/sass/editor.sass', series(stylesEditor));
  watch('src-assets/sass-partials/**/*.sass', series(stylesPartials));
  watch('static/html/*.html', series(html, reloadBrowser));
  watch('src-assets/js/vendor/*.js', series(scriptsVendor, reloadBrowser));
  watch('src-assets/js-partials/**/*.js', series(scriptPartials, reloadBrowser));
  watch('src-assets/js/main.js', series(scriptMain, reloadBrowser));
  watch('src-assets/fonts/**/*.*', series(fonts, reloadBrowser));
  watch('src-assets/img/**/*.+(png|jpg|jpeg|gif|svg)', series(images, reloadBrowser));
  watch('templates/**/*.+(php|twig)', series(reloadBrowser));

	done();
};

// Basic tasks
exports.basic = series(
  parallel(
    html,
    stylesMain,
    stylesEditor,
    stylesPartials,
    scriptsVendor,
    scriptPartials,
    scriptMain,
    fonts,
    images
  )
);

// Tasks for compiling project on CI/CD
exports.build = series(
  parallel(
    html,
    stylesMain,
    stylesEditor,
    stylesPartials,
    scriptsVendor,
    scriptPartials,
    scriptMain,
    fonts,
    images
  )
);
// Alternative name for the deployment task group
exports.deploy = series(
  exports.build
);

// Basic + Browser Sync + Watch
// Development tasks
exports.default = series(
  exports.basic,
  server,
  watchSource
);
